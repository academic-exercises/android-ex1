package com.example.tictactoe1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    boolean gameActive = true;
    Integer[] board = {null, null, null, null, null, null, null, null, null};
    // 0-X,1-O
    int activePlayer = 0;
    Integer[][] winPositions = {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8},
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8},
            {0, 4, 8}, {2, 4, 6}};
    public static int counter = 0;

    public void playerTap(View view) {
        ImageView tapImg = findViewById(view.getId());

        String imgName = view.getResources().getResourceName(view.getId());
        int tappedImage = Integer.parseInt(imgName.substring(imgName.length()-1));

        // if the tapped image is empty
        if (board[tappedImage] == null) {
            counter++;

            // check if its the last box
            if (counter == 9) {
                // reset the game
                gameActive = false;
            }

            board[tappedImage] = activePlayer;
            ImageView statusImg = findViewById(R.id.main_status_text);

            if (activePlayer == 0) {
                tapImg.setImageResource(R.drawable.x);
                statusImg.setImageResource(R.drawable.oplay);
            } else {
                tapImg.setImageResource(R.drawable.o);
                statusImg.setImageResource(R.drawable.xplay);
            }

            activePlayer = (activePlayer + 1) % 2;
        }

        boolean won = false;

        for (Integer[] winPosition : winPositions) {
            if (board[winPosition[0]] == board[winPosition[1]] &&
                board[winPosition[1]] == board[winPosition[2]] &&
                board[winPosition[0]] != null) {
                won = true;
                gameActive = false;
                ImageView statusImg = findViewById(R.id.main_status_text);

                if ( board[winPosition[0]] == 0) {
                    statusImg.setImageResource(R.drawable.xwin);
                } else {
                    statusImg.setImageResource(R.drawable.owin);
                }

                Button playAgainBtn = findViewById(R.id.main_play_again_btn);
                playAgainBtn.setVisibility(View.VISIBLE);

                break;
            }
        }

        // set the status if the match draw
        if (counter == 9 && !won) {
            ImageView statusImg = findViewById(R.id.main_status_text);
            statusImg.setImageResource(R.drawable.nowin);
            Button playAgainBtn = findViewById(R.id.main_play_again_btn);
            playAgainBtn.setVisibility(View.VISIBLE);
        }
    }

    // reset the game
    public void gameReset(View view) {
        gameActive = true;
        activePlayer = 0;
        counter = 0;

        Button playAgainBtn = findViewById(R.id.main_play_again_btn);
        playAgainBtn.setVisibility(View.INVISIBLE);

        // remove all the images from the boxes inside the grid
        for (int index = 0; index < board.length; index++) {
            board[index] = null;
            ImageView currImg = findViewById(getResources().getIdentifier("main_cell" +index, "id", getPackageName()) );
            currImg.setImageResource(R.drawable.empty);
        }

        ImageView statusImg = findViewById(R.id.main_status_text);
        statusImg.setImageResource(R.drawable.xplay);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}